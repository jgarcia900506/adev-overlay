# Copyright 1999-2017 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit eutils versionator

DESCRIPTION="Gradle Build Tool"
HOMEPAGE="https://gradle.org"
SRC_URI="
			amd64? ( https://services.gradle.org/distributions/gradle-5.2.1-all.zip -> $PN.zip )
		"

LICENSE="Apache License Version 2.0"
SLOT="$(get_version_component_range 1-2)"
KEYWORDS="~amd64"
IUSE=""

DEPEND="app-arch/zip"
RDEPEND="
		sys-libs/ncurses
		>=virtual/jdk-1.8
		x11-terms/xterm
		"

S="${WORKDIR}/gradle-${PV}"
INSTDIR="/opt/${PF}"

src_unpack() {
	unpack $PN.zip
}

src_configure() {
	sed -i "s/\(\-Xmx\)64m/\1128m/g" bin/gradle
}

src_install() {
	insinto ${INSTDIR}
	exeinto ${INSTDIR}/bin

	doexe bin/gradle
	doins -r lib media LICENSE
	dosym "${INSTDIR}/bin/gradle" "${DESTTREE}/bin/gradle"

	dodoc -r docs/*
}
