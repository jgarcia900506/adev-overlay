EAPI=7

inherit acct-user

DESCRIPTION="User for Eclipse IDE"
ACCT_USER_ID=749
ACCT_USER_GROUPS=( ${PN} )
ACCT_USER_HOME=/opt/eclipse-sdk-bin
ACCT_USER_SHELL=/sbin/nologin

acct-user_add_deps
