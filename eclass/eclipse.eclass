# Copyright 1999-2021 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

# @ECLASS: eclipse.eclass
# @MAINTAINER:
# Juan Garcia <j.garcia900506@gmail.com>
# @BLURB: install eclipse plugins.

case ${EAPI} in
	6) ;;
	*) die "${ECLASS}: EAPI ${EAPI:-0} not supported" ;;
esac

if [[ -z ${_ECLIPSE_ECLASS} ]]; then
_ECLIPSE_ECLASS=1

# @FUNCTION: installiu
# @USAGE: [features ...]
# @SUPPORTED_EAPIS: 6
# @DESCRIPTION:
# Install eclipse IU
installiu() {
	[[ -z "${INSTDIR}" ]] && die "Can't find eclipse installation."
	local repositories=$(<${INSTDIR}/repositories)

	local args=("$@")
	local features="${args[@]}"

	${INSTDIR}/eclipse \
	-vm /usr/lib64/openjdk-8/bin/java \
	-application org.eclipse.equinox.p2.director -noSplash \
	-repository ${repositories// /,} -installIU ${features// /,}
}

# @FUNCTION: uninstalliu
# @USAGE: [features ...]
# @SUPPORTED_EAPIS: 6
# @DESCRIPTION:
# Uninstall eclipse IU
uninstalliu() {
	[[ -z "${INSTDIR}" ]] && die "Can't find eclipse installation."

	local args=("$@")
	local features="${args[@]}"

	${INSTDIR}/eclipse \
	-vm /usr/lib64/openjdk-8/bin/java \
	-application org.eclipse.equinox.p2.director -noSplash \
	-uninstallIU ${features// /,}
}

# @FUNCTION: installrepo
# @USAGE: [repositories]
# @SUPPORTED_EAPIS: 6
# @DESCRIPTION:
# Install eclipse IU repository
installrepo() {
	[[ ! -f ${INSTDIR}/repositories ]] && touch ${INSTDIR}/repositories
	read -a line < ${INSTDIR}/repositories

	local args=("$@")
	local repositories="${args[@]}"

	grep -Fvq "${repositories}" ${INSTDIR}/repositories && echo "${line[@]} ${repositories}" > ${INSTDIR}/repositories
}

fi
